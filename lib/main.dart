import 'package:flutter/material.dart';

    void main() {
runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Color color = Theme.of(context).primaryColor;

    Widget buttonSection = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          _buildButtonColumn(color, Icons.ac_unit, 'Chill'),
          _buildButtonColumn(color, Icons.access_time, 'Wait'),
          _buildButtonColumn(color, Icons.local_pizza, 'Food'),
        ],
      ),
    );
    return MaterialApp(
      title: 'Layout Homework',
      theme: ThemeData(
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text('Layout Homework'),
        ),
        body: ListView(
            children: [
              Image.asset(
                'images/background.png',
                width: 600,
                height: 240,
                fit: BoxFit.cover,
              ),
            titleSection,
            buttonSection,
            textSection,
          ]
        ),
      ),
    );
  }
  Column _buildButtonColumn(Color color, IconData icon, String label) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(icon, color: color),
        Container(
          margin: const EdgeInsets.only(top: 8),
          child: Text(
            label,
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w400,
              color: color,
            ),
          ),
        ),
      ],
    );
  }
}

Widget titleSection = Container(
  padding: const EdgeInsets.all(32),
  child: Row(
    children: [
      Expanded(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: const EdgeInsets.only(bottom: 8),
              child: Text(
                'This is Teemo',
                style: TextStyle(
                  fontWeight: FontWeight.bold,),
              ),
            ),
            Text(
              'What a cute little creature',
              style: TextStyle(
                color: Colors.grey[500],
              ),
            ),
          ],
        ),
      ),
      Icon(
        Icons.home,
        color: Colors.red[500],
      ),
      Text('0'),
    ],
  ),
);

Widget textSection = Container(
  padding: const EdgeInsets.all(32),
  child: Text(
    'Undeterred by even the most dangerous and threatening of obstacles, '
        'Teemo scouts the world with boundless enthusiasm and a cheerful spirit. '
        'A yordle with an unwavering sense of morality, '
        'he takes pride in following the Bandle Scouts Code, '
        'sometimes with such eagerness that he is unaware of the broader consequences of his actions. ',
    softWrap: true,
  ),
);